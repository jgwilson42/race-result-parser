// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import com.thoughtworks.gauge.Step;
import org.junit.Assert;

public class GaugeUtils {

  /**
   * Ensure that the gauge environment is working correct.
   * @param stringOne should match the other string.
   * @param stringTwo should match the other string.
   */
  @Step("Check <stringOne> is the same as <stringTwo>")
  public void checkStringMatch(String stringOne, String stringTwo) {
    Assert.assertEquals("Check " + stringOne + " equals " + stringTwo, stringOne,
            stringTwo);
  }
}