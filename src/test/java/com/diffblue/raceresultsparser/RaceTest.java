// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;
import org.junit.Assert;

import java.util.ArrayList;

public class RaceTest {

  Races races = Races.getRaces();

  /**
   * Parse the table of data creating a new race.
   * @param data table
   * @return race
   */
  private Race parseTable(Table data) {
    Race race = new Race();
    for (TableRow row : data.getTableRows()) {
      Result result = ResultTest.parseRow(row);
      race.addResult(result);
    }
    return race;
  }

  /**
   * Check that the race results have the correct points.
   * @param data table
   * @param race to validate
   */
  private void validateResults(Table data, Race race) {
    for (TableRow row : data.getTableRows()) {
      String name = row.getCell("Name");
      String expectedPoints = row.getCell("Points");
      Assert.assertEquals(Float.parseFloat(expectedPoints), race.getResult(name).getPoints(),
              0.009f);
    }
  }

  /**
   * Setup race based on table data and add to lise of races.
   * @param raceName name of race.
   * @param data table.
   */
  @Step("Create race <raceName> <table>")
  public void createRaceResults(String raceName, Table data) {
    Race race = parseTable(data);
    races.addRace(raceName, race);
  }

  /**
   * Score the specified race.
   * @param raceName to score.
   */
  @Step("Score race <raceName>")
  public void scoreRace(String raceName) {
    Race race = races.getRace(raceName);

    race.scoreRace();

    ArrayList<Result> results = race.getResults();
    for (Result result: results) {
      Gauge.writeMessage("Person " + result.getName() + " scored " + result.getPoints());
    }
  }

  /**
   * Ensure that the race has the correct scores.
   * @param raceName to check.
   * @param data table to validate against.
   */
  @Step("Check results <raceName> <data>")
  public void checkResults(String raceName, Table data) {
    Race race = races.getRace(raceName);
    validateResults(data, race);
  }
}
