// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;
import org.junit.Assert;

public class ResultTest {

  /**
   * Create a result from the row of a table.
   * @param row from table
   * @return result
   */
  protected static Result parseRow(TableRow row) {
    Gauge.writeMessage("Name is: " + row.getCell("Name")
        + " time: " + row.getCell("Hours")
        + ":" + row.getCell("Mins")
        + ":" + row.getCell("Secs")
        + " total seconds: " + row.getCell("TotalSeconds"));
    Result result = new Result();
    result.setName(row.getCell("Name"));
    result.setSeconds(Integer.parseInt(row.getCell("Hours")),
        Integer.parseInt(row.getCell("Mins")),
        Integer.parseInt(row.getCell("Secs")));
    return result;
  }

  /**
   * Iterate through table of valid results.
   * Check that total seconds are calculated correctly.
   * @param data table
   */
  @Step("Check the following valid results <table>")
  public void createValidResults(Table data) {
    for (TableRow row : data.getTableRows()) {
      Result result = parseRow(row);
      Assert.assertEquals(Integer.parseInt(row.getCell("TotalSeconds")),
              result.getSeconds());
    }
  }

  /**
   * Iterate through table of invalid results.
   * Check that each result throws an exception.
   * @param data table
   */
  @Step("Check the following invalid results <table>")
  public void createInvalidResults(Table data) {
    for (TableRow row : data.getTableRows()) {
      Result result = new Result();
      boolean exceptionThrown = false;
      try {
        result = parseRow(row);
      } catch (IllegalArgumentException exception) {
        exceptionThrown = true;
      } finally {
        Assert.assertTrue("Illegal argument exception should have "
                + "been thrown for invalid input", exceptionThrown);
      }
    }
  }

}