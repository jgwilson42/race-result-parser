// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;

public class AthletesTest {

  /**
   * Add the athletes specified to the list of known athletes.
   * @param data table.
   */
  @Step("Register known athletes <data>")
  public void addAthletes(Table data) {
    Athletes athletes = Athletes.getInstance();

    for (TableRow row : data.getTableRows()) {
      athletes.addName(row.getCell("Name"));
    }
  }
}
