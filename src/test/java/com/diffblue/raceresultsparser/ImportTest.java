// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;
import org.junit.Assert;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ImportTest {

  Races races = Races.getRaces();

  /**
   * Load the results from the file specified.
   * @param raceName to use in races.
   * @param filename to import.
   */
  @Step("Load results for <raceName> from file <fileName>")
  public void loadScoreResults(String raceName, String filename) {
    // TODO: Use proper separator
    Path file = Paths.get("src/test/data/" + filename);
    Race race = new Race();
    Parser parser;

    if (filename.endsWith("csv")) {
      //StuWeb Style race results
      parser = new StuWebParser();
    } else if (filename.endsWith("xlsx")) {
      // Race Ahead Style race results.
      parser = new RaceAheadParser();
    } else {
      throw new IllegalArgumentException("Filename" + filename
          + " does not have a valid extension");
    }

    // Get the results, score them and add them to the list of races.
    race = parser.parse(file);
    race.scoreRace();
    races.addRace(raceName, race);

    // Write out details for debugging.
    for (Result result : race.getResults()) {
      if (result.getTeamCherwell()) {
        Gauge.writeMessage("Competitor " + result.getName() + " scored " + result.getPoints());
      }
    }
  }


}
