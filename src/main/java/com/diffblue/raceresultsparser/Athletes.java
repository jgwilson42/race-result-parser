// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import java.util.HashSet;

public class Athletes {

  private HashSet<String> athleteNames = new HashSet<>();
  private static Athletes athletes;

  private Athletes() {
    // Singleton
  }

  /**
   * Create and return instance of the athletes.
   * @return athletes.
   */
  public static Athletes getInstance() {
    if (athletes == null) {
      athletes = new Athletes();
    }
    return athletes;
  }

  public void addName(String name) {
    athleteNames.add(name);
  }

  public HashSet getNames() {
    return athleteNames;
  }

  /**
   * Is the name in the list of athletes.
   * @param name to check for
   * @return is in the list?
   */
  public boolean isNameInList(String name) {
    for (String athlete : athleteNames) {
      if (name.toLowerCase().equals(athlete.toLowerCase())) {
        return true;
      }
    }
    return false;
  }
}
