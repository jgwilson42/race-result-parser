// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

public class Result implements Comparable<Result> {

  private String name;
  private int seconds;
  private float points;
  private boolean teamCherwell;

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  /**
   * Calculate and set the seconds of the result.
   * @param hours taken
   * @param minutes taken
   * @param seconds taken
   */
  public void setSeconds(int hours, int minutes, int seconds) {

    if (hours > 18) {
      throw new IllegalArgumentException("Hours is " + hours + ", cannot be more than 18");
    }

    if (minutes > 59) {
      throw new IllegalArgumentException("Minutes is " + minutes + ", cannot be more than 59");
    }

    if (seconds > 59) {
      throw new IllegalArgumentException("Seconds is " + seconds + ", cannot be more than 59");
    }

    int newSeconds = seconds + (minutes * 60) + (hours * 60 * 60);

    if (newSeconds < 1) {
      throw new IllegalArgumentException("Total time is " + seconds
              + ", cannot be less than 1 second");
    }

    this.seconds = newSeconds;
  }

  public int getSeconds() {
    return seconds;
  }

  public void setPoints(float points) {
    this.points = points;
  }

  public float getPoints() {
    return points;
  }

  public void setTeamCherwell(boolean teamCherwell) {
    this.teamCherwell = teamCherwell;
  }

  public boolean getTeamCherwell() {
    return teamCherwell;
  }

  @Override
  public int compareTo(Result result) {
    if (this.getSeconds() < result.getSeconds()) {
      return -1;
    } else if (this.getSeconds() == result.getSeconds()) {
      return 0;
    }
    return 1;
  }
}
