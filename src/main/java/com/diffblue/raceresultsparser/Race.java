// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import java.util.ArrayList;

public class Race {

  private ArrayList<Result> results;

  public ArrayList<Result> getResults() {
    return results;
  }

  public void setResults(ArrayList<Result> results) {
    this.results = results;
  }

  /**
   * Add result to the race.
   * @param result to be added.
   */
  public void addResult(Result result) {
    if (results == null) {
      results = new ArrayList<>();
    }
    results.add(result);
  }

  /**
   * Get the result for a particular competitor.
   * @param name of competitor.
   * @return their result.
   */
  public Result getResult(String name) {
    for (Result result : results) {
      if (name.toLowerCase().equals(result.getName().toLowerCase())) {
        return result;
      }
    }
    throw new IllegalArgumentException("Competitor " + name + " not in the results");
  }

  /**
   * Calculate the points for each competitor.
   */
  public void scoreRace() {

    results.sort(Result::compareTo);

    int baseResultPosition = ((int) Math.round(0.4 * results.size()) - 1);
    int baseResultSeconds = results.get(baseResultPosition).getSeconds();

    for (Result result : results) {
      float points = ((float) baseResultSeconds / (float) result.getSeconds()) * 100;
      result.setPoints(points);
    }
  }


}
