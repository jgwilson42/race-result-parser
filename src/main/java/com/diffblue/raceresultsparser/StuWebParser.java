// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;

public class StuWebParser implements Parser {

  /**
   * Parse results from StuWeb.
   * @param file the path to the CSV export.
   * @return the results parsed.
   */
  public Race parse(Path file) {
    Race race = new Race();

    Reader reader;
    CSVParser csvParser;

    try {
      reader = Files.newBufferedReader(file);
      csvParser = new CSVParser(reader, CSVFormat.DEFAULT);
    } catch (IOException exception) {
      System.out.println("ERROR: Could not open file.");
      exception.printStackTrace();
      return null;
    }

    for (CSVRecord csvRecord : csvParser) {

      /*
       Format of the file is:
       0 - Position
       1  - Bib #
       2  - Athlete Name
       3  - Gender
       4  - Category
       5  - Club
       6  - Swim
       7  - T1
       8  - Cycle
       9  - T2
       10 - Run
       11 - Gun Time
       12 - Time Difference
       */

      try {
        if (!csvRecord.get(0).equals("Position")
                && !csvRecord.get(0).contains("Copyright")
                && !csvRecord.get(0).contains("Not for re-publication")) {

          Result result = new Result();
          result.setName(csvRecord.get(2));
          result.setTeamCherwell(csvRecord.get(5).toLowerCase().equals("team cherwell")
                  || athletes.isNameInList(result.getName()));
          String[] time = csvRecord.get(11).split(":");
          result.setSeconds(Integer.parseInt(time[0]),
                  Integer.parseInt(time[1]), Integer.parseInt(time[2]));
          race.addResult(result);
        }
      } catch (Exception ex) {
        System.out.println("WARNING: Cannot parse record: " + csvRecord.toString());
      }
    }

    return race;
  }
}
