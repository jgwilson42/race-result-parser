// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import java.util.HashMap;

public class Races {

  private HashMap<String, Race> races = new HashMap<>();
  private static Races instance;

  private Races() {
    // Don't allow construction
  }

  /**
   * Setup and return instance of Races.
   * @return Series of races.
   */
  public static Races getRaces() {
    if (instance == null) {
      instance = new Races();
    }
    return instance;
  }

  public void addRace(String name, Race race) {
    races.put(name, race);
  }

  public Race getRace(String name) {
    return races.get(name);
  }
}
