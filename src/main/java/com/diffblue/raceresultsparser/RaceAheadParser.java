// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;


public class RaceAheadParser implements Parser {

  @Override
  public Race parse(Path file) {

    /* Format of the file is:
      0  - Bib
      1  - Overall Position
      2  - Name
      3  - Chiptime
      4  - Category
      5  - Category Position
      6  - Team / Club
      7  - Gender
      8  - Gender Position
      9  - Avg speed
      10 - Avg pace
      11 - Status
      12 - Leg - Swim - Duration
      13 - Leg - Swim - Cumulative time
      14 - Leg - Swim - Rank
      15 - Leg - T1 - Duration
      16 - Leg - T1 - Cumulative time
      17 - Leg - T1 - Rank
      18 - Leg - Bike - Duration
      19 - Leg - Bike - Cumulative time
      20 - Leg - Bike - Rank
      21 - Leg - T2 - Duration
      22 - Leg - T2 - Cumulative time
      23 - Leg - T2 - Rank
      24 - Leg - Run - Duration
      25 - Leg - Run - Cumulative time
      26 - Leg - Run - Rank
     */

    FileInputStream fileInputStream;
    XSSFWorkbook workbook;

    try {
      fileInputStream = new FileInputStream(file.toFile());
      workbook = new XSSFWorkbook(fileInputStream);
    } catch (IOException exception) {
      System.out.println("ERROR: Could not open file.");
      exception.printStackTrace();
      return null;
    }

    // Assume that there is only one worksheet
    XSSFSheet sheet = workbook.getSheetAt(0);
    Race race = new Race();

    Iterator<Row> rowIterator = sheet.iterator();
    while (rowIterator.hasNext()) {
      Row row = rowIterator.next();
      Result result = new Result();

      // Avoid the header row, or rows where the Name is blank
      if (!row.getCell(2).getStringCellValue().equals("Name")
            && row.getCell(2).CELL_TYPE_BLANK != 1) {
        result.setName(row.getCell(2).getStringCellValue());
        // Check for people with team name "team cherwell"
        if (row.getCell(6).CELL_TYPE_STRING == 1 && row.getCell(6) != null) {
          result.setTeamCherwell(row.getCell(6).getStringCellValue().toLowerCase()
              == "team cherwell");
        }
        // Get the chiptime for their result
        Date time = row.getCell(3).getDateCellValue();
        Calendar timeCal = Calendar.getInstance();
        timeCal.setTime(time);

        // Exclude results for people that did not start or did not finish
        if (timeCal.get(Calendar.HOUR) != 0 || timeCal.get(Calendar.MINUTE) != 0
            || timeCal.get(Calendar.SECOND) != 0) {
          result.setSeconds(timeCal.get(Calendar.HOUR), timeCal.get(Calendar.MINUTE),
              timeCal.get(Calendar.SECOND));
          race.addResult(result);
        }
      }
    }
    return race;
  }
}
