// Copyright Diffblue Ltd. 2018-2019. All rights reserved.

package com.diffblue.raceresultsparser;

import java.nio.file.Path;

public interface Parser {

  Athletes athletes = Athletes.getInstance();

  public Race parse(Path file);

}
