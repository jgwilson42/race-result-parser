# Score race from Excel (XLSX) format

Race Ahead provide results in a XLSX format.

## Score 2019 Woburn Abbey (just registered as Team Cherwell)

 * Load results for "Woburn Abbey" from file "Results woburn-abbey-triathlon-2019-saturday.xlsx"
 * Score race "Woburn Abbey"
 * Check results "Woburn Abbey"
   | Name           | Points |
   |----------------|--------|
   | Adam Prewer    | 111.09 |
   | Doug Rose      | 78.57  |


Copyright Diffblue Ltd. 2018-2019. All rights reserved.
