# Score a race given raw results

This file checks that a race can be scored given a set of results

## Check valid and invalid results

* Check the following valid results
  | Name | Hours | Mins | Secs | TotalSeconds |
  |------|-------|------|------|--------------|
  | foo  | 0     | 0    | 1    | 1            |
  | foo  | 1     | 0    | 0    | 3600         |
  | foo  | 0     | 1    | 0    | 60           |
  | foo  | 18    | 59   | 59   | 68399        |

* Check the following invalid results
  | Name | Hours | Mins | Secs |
  |------|-------|------|------|
  | foo  | 0     | 0    | 0    |
  | foo  | 19    | 0    | 0    |
  | foo  | 0     | 60   | 0    |
  | foo  | 0     | 0    | 60   |


## Simple race, all members are Team Cherwell

* Create race "TestRace1"
  | Name | Hours | Mins | Secs |
  |------|-------|------|------|
  | foo1 | 1     | 1    | 1    |
  | foo2 | 2     | 2    | 2    |
  | foo3 | 3     | 3    | 3    |
  | foo4 | 4     | 4    | 4    |
  | foo5 | 5     | 5    | 5    |
  | foo6 | 6     | 6    | 6    |
  | foo7 | 7     | 7    | 7    |
  | foo8 | 8     | 8    | 8    |
  | foo9 | 9     | 9    | 9    |
  | foo10| 10    | 10   | 10   |

* Score race "TestRace1"

* Check results "TestRace1"
  | Name | Points |
  |------|--------|
  | foo1 | 400    |
  | foo2 | 200    |
  | foo3 | 133.33 |
  | foo4 | 100    |
  | foo5 | 80     |
  | foo6 | 66.67  |
  | foo7 | 57.14  |
  | foo8 | 50     |
  | foo9 | 44.44  |
  | foo10| 40     |


Copyright Diffblue Ltd. 2018-2019. All rights reserved.