# Score race from CSV format

StuWeb provide results in a CSV format.

## Score 2019 Banbury Triathlon (just registered as Team Cherwell)

 * Load results for "Banbury" from file "stuweb_results-8743.csv"
 * Score race "Banbury"
 * Check results "Banbury"
   | Name              | Points |
   |-------------------|--------|
   | Jacqui Smith      | 89.15  |
   | Caroline Chanides | 95.34  |
   | Denise Shepherd   | 108.75 |
   | Kim Shaw          | 103.31 |
   | Nick Donkin       | 120.26 |
   | Andy Macdonald    | 116.89 |
   | Marc Phillpot     | 86.39  |
   | Stephen Beck      | 110.55 |
   | Emmanuel Heraud   | 118.59 |
   | Doug Rose         | 84.35  |

## Score 2019 Banbury Triathlon (all TC Athletes)

 * Register known athletes
   | Name             |
   |------------------|
   | James Wilson     |
   | Andrew Sleight   |
   | Heidi Yates      |
   | Chris Bagnall    |
   | David Shepherd   |
   | Steve Parnell    |
   | Henry Sleight    |
   | Lee Davis        |
 * Load results for "Banbury" from file "stuweb_results-8743.csv"
 * Score race "Banbury"
 * Check results "Banbury"
   | Name              | Points |
   |-------------------|--------|
   | Jacqui Smith      | 89.15  |
   | Caroline Chanides | 95.34  |
   | Denise Shepherd   | 108.75 |
   | Kim Shaw          | 103.31 |
   | Nick Donkin       | 120.26 |
   | Andy Macdonald    | 116.89 |
   | Marc Phillpot     | 86.39  |
   | Stephen Beck      | 110.55 |
   | Emmanuel Heraud   | 118.59 |
   | Doug Rose         | 84.35  |
   | Henry Sleight     | 139    |
   | Heidi Yates       | 108.17 |
   | Chris Bagnall     | 82.96  |
   | David Shepherd    | 82.48  |

Copyright Diffblue Ltd. 2018-2019. All rights reserved.
